require("rbokeh")
library("ggplot2")
require("RCurl")
require("jsonlite")
require("dplyr")
library("lubridate")
library("dygraphs")
library("xts")
library("plotly")


setwd("C:/Users/jenng/OneDrive/School/Class of 2017 (UT)/CS 329E - Data Visualization/Projects/dv_finalproject_quynhnguyen/03Shiny")

# # Read in data from DB
# 
# walmart_features <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from WALMART_FEATURES"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mm73828', PASS='orcl_mm73828', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE)))
# 
# walmart_stores <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from WALMART_STORES"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mm73828', PASS='orcl_mm73828', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE)))
# 
# # Here the data is retrieved in batches because else it's takes to long for the REST server to respond
# 
# walmart_train1 <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from WALMART_TRAIN where store_id in (1,2,3,4,5,6)"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mm73828', PASS='orcl_mm73828', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE)))
# walmart_train2 <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from WALMART_TRAIN where store_id in (7,8,9,10,11,12)"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mm73828', PASS='orcl_mm73828', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE)))
# #walmart_train3 <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from WALMART_TRAIN where store_id in (13,14,15,16,17,18)"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mm73828', PASS='orcl_mm73828', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE)))
# #walmart_train4 <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from WALMART_TRAIN where store_id in (19,20,21,22,23,24)"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mm73828', PASS='orcl_mm73828', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE)))
# #walmart_train5 <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from WALMART_TRAIN where store_id in (25,26,27,28,29,30)"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mm73828', PASS='orcl_mm73828', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE)))
# 
# # Combine data
# walmart_train = rbind(walmart_train1, walmart_train2)
# 
# #walmart_train = rbind(walmart_train1, walmart_train2, walmart_train3, walmart_train4, walmart_train5)
# 
# # Reformat the date format and get ready to join
# #walmart_features$DATE_OF_WEEK <- as.Date(walmart_features$DATE_OF_WEEK, "%m/%d/%Y")
# #walmart_train$DATE_OF_WEEK <- as.Date(walmart_train$DATE_OF_WEEK, "%Y-%m-%d")
# 
# # Join
# data = merge(merge(walmart_train, walmart_features, by=c("STORE_ID","DATE_OF_WEEK"), all=FALSE),walmart_stores, by="STORE_ID")
# 
# data$ISHOLIDAY = data$ISHOLIDAY.x
# data$ISHOLIDAY.x = NULL
# data$ISHOLIDAY.y = NULL

df = readRDS('../01 Data/walmart_data.rds') 

# Create a new column that aggregates all the markdown column 
# to see the total effect on having promotional sales 
df$total_markdown = as.numeric(df$MARKDOWN1) + as.numeric(df$MARKDOWN2) + as.numeric(df$MARKDOWN3) + as.numeric(df$MARKDOWN4) + as.numeric(df$MARKDOWN5) 
df$CPI = as.numeric(df$CPI) 
df$UNEMPLOYMENT = as.numeric(df$UNEMPLOYMENT) 
df$DEPT = as.character(df$DEPT)
# Filter to only when it is not holiday 
# in order to see if having promotions will correlate to higher sales despite not being a holiday 
data_store_1 <- df %>% filter(ISHOLIDAY == "FALSE") %>% group_by(DATE_OF_WEEK) %>% summarise(  
  sum_markdown = mean(total_markdown),  
  avg_fuelprice = mean(FUEL_PRICE, na.rm = TRUE), 
  avg_cpi = mean(CPI, na.rm = TRUE),  
  avg_temperature = mean(TEMPERATURE, na.rm = TRUE), 
  avg_unemployment = mean(UNEMPLOYMENT, na.rm = TRUE), 
  avg_sales = mean(WEEKLY_SALES, na.rm = TRUE)) %>% mutate(month = month(DATE_OF_WEEK))

months <- c(1,2,3,4,5,6,7,8,9,10,11,12)
colors <- c("#a6cee3", "#1f78b4", "#fdbf6f", "#b2df8a", "#33a02c",
            "#bbbb88", "#baa2a6", "#e08e79", "#d1c6b2", "#391e16", 
            "#28f328", "#5c1fd1")
data_store_1$color <- colors[match(data_store_1$month,months)]

p1 <- figure() %>% 
ly_points(data_store_1$avg_cpi, data_store_1$avg_sales, data = data_store_1$DATE_OF_WEEK, color = data_store_1$color, hover = list(data_store_1$avg_cpi, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
  tool_box_select() %>% 
  tool_lasso_select() %>% 
  x_axis(label = "CPI") %>% 
  y_axis(label = "Weekly Sales")

p2 <- figure() %>% 
  ly_points(data_store_1$avg_unemployment, data_store_1$avg_sales, data = data_store_1$DATE_OF_WEEK,color = data_store_1$color,hover = list(data_store_1$avg_unemployment, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
  tool_box_select() %>% 
  tool_lasso_select() %>% 
  x_axis(label = "Unemployment") %>% 
  y_axis(label = "Weekly Sales")

p3 <- figure() %>% 
  ly_points(data_store_1$avg_temperature, data_store_1$avg_sales,  data = data_store_1$DATE_OF_WEEK,color = data_store_1$color, hover = list(data_store_1$avg_temperature, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
  tool_box_select() %>% 
  tool_lasso_select() %>% 
  x_axis(label = "Temperature") %>% 
  y_axis(label = "Weekly Sales")
p4 <- figure() %>% 
  ly_points(data_store_1$avg_fuelprice, data_store_1$avg_sales,  data = data_store_1$DATE_OF_WEEK,color = data_store_1$color, hover = list(data_store_1$sum_markdown, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
  tool_box_select() %>% 
  tool_lasso_select() %>% 
    x_axis(label = "Average Fuel Price") %>% 
  y_axis(label = "Weekly Sales")

p5 <- figure() %>% 
  ly_points(data_store_1$sum_markdown, data_store_1$avg_sales,  data = data_store_1$DATE_OF_WEEK,color = data_store_1$color, hover = list(data_store_1$sum_markdown, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
  tool_box_select() %>% 
  tool_lasso_select() %>% 
  x_axis(label = "Average Promotion") %>% 
  y_axis(label = "Weekly Sales")

p6 <- figure(xaxes = FALSE) %>% 
  ly_crect(x=colors, y=months, color = colors, hover=months) %>% 
  ly_text(x=colors, y=months, text = months, font_style = "bold", font_size = "10pt", align = "center", baseline = "middle")  

grid_plot(list(p1,p2,p3,p4,p5,p6), width =1200, height=1500, same_axes = FALSE, link_data = TRUE, nrow=3)

# %>% ly_text(data_store_1$avg_cpi, data_store_1$avg_sales, text = data_store_1$month,
#font_style = "bold", font_size = "10pt",
#align = "left", baseline = "middle") 

data_store_1$DATE_OF_WEEK <- as.Date(as.character(data_store_1$DATE_OF_WEEK),format = "%Y-%m-%d")
x <- xts(data_store_1[,-1], order.by=as.POSIXct(data_store_1$DATE_OF_WEEK))


n <- 5

avg_sales_df <- df %>% group_by(STORE_ID) %>% summarise(AVG_SALES=mean(WEEKLY_SALES)) %>% arrange(desc(AVG_SALES))
top_n_stores <- as.vector(head(avg_sales_df,n)$STORE_ID)
all_stores <- unique(avg_sales_df$STORE_ID)
not_top_n <- setdiff(all_stores, top_n_stores)
d4 <- df

ggplot(d4, aes(TYPE_OF_STORE, STORE_ID)) +
  labs(title='Weekly Average Sales based on Store ID and Holiday For Store')+
  geom_tile(aes(fill = WEEKLY_SALES), colour = "grey50") +
  geom_text(size = 3, aes(fill = floor(WEEKLY_SALES), label = floor(WEEKLY_SALES)))

n <- 10
dept_sales_df <- df %>% group_by(DEPT) %>% summarise(AVG_SALES=mean(WEEKLY_SALES)) %>% arrange(desc(AVG_SALES))
top_n_dept <- as.vector(head(dept_sales_df,n)$DEPT)
all_dept <- unique(dept_sales_df$DEPT)
dept_sales_df <- dept_sales_df %>% arrange((AVG_SALES))
bottom_n_dept <- as.vector(head(dept_sales_df,n)$DEPT)

newdf <- df %>% filter(DEPT %in% c(top_n_dept,bottom_n_dept)) %>% mutate(classification=ifelse(DEPT %in% top_n_dept, "x in top", "x  in bottom"))

newdf2 <- df %>% filter(DEPT %in% c(top_n_dept,bottom_n_dept)) %>% mutate(classification=ifelse(DEPT %in% top_n_dept, "x in top", "x  in bottom"))

plot_ly(y = newdf$total_markdown, x= newdf$DEPT, type = "box")

plot_ly(y = newdf2$total_markdown, x= newdf2$DEPT, color=newdf2$classification,type = "box")

lungDeaths <- cbind(mdeaths, fdeaths)
dygraph(x)

var_temperature <- var(data_store_1$avg_temperature)
var_cpi <- var(data_store_1$avg_cpi)
var_fuelprice <- var(data_store_1$avg_fuelprice)
var_unemployment <- var(data_store_1$avg_unemployment)

cov(data_store_1$avg_cpi, data_store_1$avg_sales)  
cov(data_store_1$avg_fuelprice, data_store_1$avg_sales)  
cov(data_store_1$avg_temperature, data_store_1$avg_sales)  
cov(data_store_1$avg_unemployment, data_store_1$avg_sales)  
