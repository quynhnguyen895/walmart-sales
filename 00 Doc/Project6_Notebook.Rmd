---
title: "Walmart Sales In Relation To Store Identity and Regional Activities"
author: "Quynh Nguyen (qmn76), Hanna Jones (hmj427), Mark Metzger (mm73828)"
date: "November 14, 2016"
output:
  html_document:
    toc: yes
  html_notebook:
    toc: yes
  pdf_document:
    toc: yes
---
## Session Info

```{r}
sessionInfo()
```

## Finding & Processing Data

### STEP 1: Find a dataset. 

Public datasets are available on sites such as the [United States Census Bureau](http://www.census.gov/), [Reddit](https://www.reddit.com/r/datasets), and [UCI](https://archive.ics.uci.edu/ml/datasets/).

In this case, we a found a set of data that Walmart published for Kaggle, a data science competition site, that includes store information, historic weekly sales, miscillaneous factor that includes sale for 45 stores. The original data set is published [here](https://www.kaggle.com/c/walmart-recruiting-store-sales-forecasting/data).

Unlike the data set we used previously, the data is broken into many files/tables. The three tables that we used for this project are:

+ stores.csv (includes anonymized information about the stores)
+ train.csv (incldues historical training date from 2010-02-05 to 2012-11-01) 
+ features.csv (includes misc. information related to store, department, regional activity for each week)

Here's the description for each column name in the data file.

##### Train.csv

Name of Column | Description
------------- | -------------
Store | the store number
Dept | the department number
Date | the week
Weekly_Sales | sales for the given department in the given store
IsHoliday | whether the week is a special holiday week
```{r}
read.csv("../01 Data/train.csv", stringsAsFactors = FALSE, nrows=20)
```

##### Stores.csv

Name of Column | Description
------------- | -------------
Store | the store number
Type | type of store
Size | size of store
```{r}
read.csv("../01 Data/stores.csv", stringsAsFactors = FALSE, nrows=20)
```

##### Features.csv

Name of Column | Description
------------- | -------------
Store | the store number
Date | the week
Temperature | average temperature in the region
Fuel_Price | cost of fuel in the region
MarkDown1-5 | anonymized data related to promotional markdowns that Walmart is running. MarkDown data is only available after Nov 2011, and is not available for all stores all the time. Any missing value is marked with an NA.
CPI | the consumer price index
Unemployment | the unemployment rate
IsHoliday | whether the week is a special holiday week

```{r}
read.csv("../01 Data/features.csv", nrows=20, stringsAsFactors = FALSE)
```

### STEP 2: Clean up the data Using Extract, Transform, and Load techniques.

The process of ETL is straightforward. We read in the file, figure out what columns are considered dimensions and measured and process numbers as needed.

Initially when we first processed the data, one of our columns is a date with the format "MM/DD/YYYY". At first, we imported it like normal, interpreting the column as a varchar2(4000). At this point, querying data and joining the tables in Oracle would work fine. However, Tableau has some trouble recognizing the formatting with its default query so we couldn't change column from a string type to a date time, which prevents us from being to join the table all together.

![](../00 Doc/errorInTableau.png)

So we went back to fix the data and reformat the columns again. We added a piece of code that reformat the "MM/DD/YYYY" to "YYYY-MM-DD".

```
train_df$Date <- as.Date(train_df$Date, format = "%m/%d/%Y") 
```

Additionally, we made sure the date column would be classified as a DATE type in oracle instead of a string.

```
for(d in dimensions) {
  # If column is a date, change it to DATE type instead of a string.
  if (d == "Date"){
    sql <- paste(sql, paste("Date_of_week", "DATE DEFAULT (sysdate),\n"))
  }
  else{
    sql <- paste(sql, paste(d, "varchar2(4000),\n"))
  }
```

After running the R_ETL script, we copy & paste the cat(sql) result and run it in SQL Developer to create new tables. One thing to note that some names are not valid as column names in SQL so we changed them. For example, Store is renamed to Store_Id, Date is renamed to Date_of_Week, and so on. To demonstrate, below are our final SQL codes to create the tables.

```
CREATE TABLE walmart_train (
-- Change table_name to the table name you want.
 Store_Id varcatehar2(4000),
 Dept varchar2(4000),
 Date_of_week DATE DEFAULT (sysdate),
 IsHoliday varchar2(4000),
 Weekly_Sales number(38,4)
 );
```

```
 CREATE TABLE walmart_features (
-- Change table_name to the table name you want.
 Type_of_store varchar2(4000),
 Store_id varchar2(4000),
 Date_of_week DATE DEFAULT (sysdate),
 Temperature number(38,4),
 Fuel_Price number(38,4),
 MarkDown1 number(38,4),
 MarkDown2 number(38,4),
 MarkDown3 number(38,4),
 MarkDown4 number(38,4),
 MarkDown5 number(38,4),
 CPI number(38,4),
 Unemployment number(38,4),
 IsHoliday varchar2(4000)
 );
```

```
 CREATE TABLE walmart_stores (
-- Change table_name to the table name you want.
 Type_of_store varchar2(4000),
 Store_id varchar2(4000),
 Size_of_store number(38,4)
 );
```

In addition, the R_ETL also outputs a reformatted version where the data is cleaned up nicely to meet the standards for importing to SQL database, which we then use to import in the databse. During the importing stage, it's neccessary to also match the old column names to the new column names we dictated when we first create the empty table. We also made sure to match the date format to the format in Oracle.

![](../00 Doc/dateOfWeek.png)

### STEP 3A: Retrieving Data with R

After the data is succesfully imported to the database. We then retrieve it using R. Note that the tables are fairly big (e.g. 420,000+ rows for the train table) so we split the query into smaller batches to prevent time-outs from stoping the data retrieval process.

Then we bind these smaller WALMART_TRAIN batches together vertically, and joined them with the WALMART_FEATURES and WALMART_STORES by the STORE_ID, DATE_OF_WEEK.

```{r}
source("../01 Data/Data.R", echo = TRUE)
```


### STEP 3B: Retrieving Data with Tableau

As we mentioned before, initially there was a problem joining the table because Tableau has trouble casting the Date column as a date and not a string, which prevents us from being able to join the table by dates. Therefore, we used an SQL query as an alternative to gather data from all three tables using inner join in Tableau is a little bit different from the sql query for R.

```
select walmart_train.store_id, 
walmart_train.dept, 
TO_DATE(walmart_train.DATE_OF_WEEK,'YYYY-MM-DD') as real_date,
walmart_train.DATE_OF_WEEK,
walmart_train.isholiday, 
walmart_train.weekly_sales, 
walmart_stores.type_of_store, 
walmart_stores.size_of_store, 
walmart_features.fuel_price,
walmart_features.temperature,
walmart_features.cpi,
walmart_features.unemployment
from walmart_train, walmart_stores, walmart_features
where walmart_train.store_id = walmart_stores.store_id
and walmart_train.store_id = walmart_features.store_id
and walmart_train.DATE_OF_WEEK = TO_CHAR(TO_DATE(walmart_features.DATE_OF_WEEK,'MM/DD/YYYY'), 'YYYY-MM-DD')
```
Then, in order to not run into any error in Tableau, we have made a another column called **real_date** column where we convert the format of the string to YYYY-MM-DD. If not, we will run into an error where Tableau has a problem transforming and retrieving data with the query.

However, after re-formatting the table and updating the ORACLE database, Tableau was able to recognize the DATE_OF_WEEK column as dates and join the table easily.

![](../00 Doc/tableauTables.png)

### Step 4: Interesting Visualizations

#### Holiday Sales Analysis

![](../02 Tableau/Overall_Sales_Trends.png)

First, we mapped a time series of each of the Wal Mart stores' sales in our dataset, and found an overall trend -- sales increase in the month of December across the board. This trend is to be expected. However, we pressed further into our analysis of the sales trends utilizing our WALMART_STORES table column "Type of Store."

![](../02 Tableau/Average_Sales_Trends.png)

While our dataset was not clear as to what Stores A, B, and C mean, we were able to graph the average sales grouped by Type of Store and came to the conclusion that Type A stores are probably larger stores in large cities, Type B stores are characterized by average sales, and Type C stores are smaller stores that do not make as much revenue.

This graph also offered a curious notion - that Type C stores operate against the sales trends of the other two types. At first, we considered there could be an outlier store that skewed the data, so we separated our overall sales trends graph from before into each type of store.

![](../02 Tableau/Sales_Trends_A.png)

![](../02 Tableau/Sales_Trends_B.png)

![](../02 Tableau/Sales_Trends_C.png)

As evident from these graphs, Type C stores are not only following this trend of increasing sales during the holidays, but their sales actually DECREASE during these months.

![](../02 Tableau/Store_43_Sept2010.png)

For example, if you hover over Store 43, a Type C store, its sales in September are on average $15,000 a week.

![](../02 Tableau/Store_43_Dec2010.png)

But in December, this dips down to around $12,500. This is contrary to what you would expect of a Wal Mart, especially given that Wal Mart is one of the most popular places to shop during the holidays.

![](../02 Tableau/Average_Markdowns_By_Store.png)

After messing around with the data to discover why this may be, we came across this visualization that uses the "MARKDOWN" columns from our FEATURES table. Markdowns refer to promotional markdowns that Walmart is running. Higher markdowns means more promotions and discounts on their items.

Interestingly, the average markdowns for C Stores is significantly lower than other stores.

What we have assumed from this visualization is that customers that normally visit Type C stores end up purchasing items at other, larger, Wal Marts during the holidays in order to have a wider selection and more promotional discounts on their purchases. Thus, while these larger stores (A and B) are making more sales during the holidays, Type C stores are losing customers.

#### [Shiny Application](https://cs329equynhmarkhanna.shinyapps.io/finalproject/)

#### Factors Attributing to Sales Analysis

With our interpretation that it's not the holidays that are driving sales, but rather the promotional sales, we were interested in finding out what of the following factors weigh more when it comes to driving sales: Consumer Price Index, unemployment rate, temperature, fuel price, or average promotion.

To analyze so, we chose to look at data where it's not a holiday. Then we averaged the each of these values for every week using dplyr.

```
  ## Create a new total markdown column that is a sum of all 5 markdown columns
  df$total_markdown = as.numeric(df$MARKDOWN1) + as.numeric(df$MARKDOWN2) + as.numeric(df$MARKDOWN3) + as.numeric(df$MARKDOWN4) + as.numeric(df$MARKDOWN5) 
  df$CPI = as.numeric(df$CPI) 
  df$UNEMPLOYMENT = as.numeric(df$UNEMPLOYMENT) 
  df$DEPT = as.character(df$DEPT)
  
  # Filter to only when it is not holiday 
  # in order to see if having promotions will correlate to higher sales despite not being a holiday 
  data_store_1 <- df %>% filter(ISHOLIDAY == "FALSE") %>% group_by(DATE_OF_WEEK) %>% summarise(  
    sum_markdown = mean(total_markdown),  
    avg_fuelprice = mean(FUEL_PRICE, na.rm = TRUE), 
    avg_cpi = mean(CPI, na.rm = TRUE),  
    avg_temperature = mean(TEMPERATURE, na.rm = TRUE), 
    avg_unemployment = mean(TEMPERATURE, na.rm = TRUE), 
    avg_sales = mean(WEEKLY_SALES, na.rm = TRUE)) %>% mutate(month = month(DATE_OF_WEEK)) # Create a new column that is month for graphings
```
Finally, we colored them by month. Since rbokeh by default doesn't have an option to create legends for all 12 months (instead, it bins the values to every 2 or 3 months), we have manually created a legend with a individual color for everymonth. Here, we are created a vector that match each month to color in the dataframe.

```
  months <- c(1,2,3,4,5,6,7,8,9,10,11,12)
  colors <- c("#a6cee3", "#1f78b4", "#fdbf6f", "#b2df8a", "#33a02c",
              "#bbbb88", "#baa2a6", "#e08e79", "#d1c6b2", "#391e16", 
              "#28f328", "#5c1fd1")
  data_store_1$color <- colors[match(data_store_1$month,months)]
```

Finally, we created 5 individual plots pairing weekly sales with a factor. We decided to color the data points by month and show the date of the week as you hover. Then, we put them on the same grid of 3 rows.

Server.R
```
  output$scatterPlot <- renderRbokeh({
    
    p1 <- figure() %>% 
      ly_points(data_store_1$avg_cpi, data_store_1$avg_sales, data = data_store_1$DATE_OF_WEEK, color = data_store_1$color, hover = list(data_store_1$avg_cpi, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
      tool_box_select() %>% 
      tool_lasso_select() %>% 
      x_axis(label = "CPI") %>% 
      y_axis(label = "Weekly Sales")
    
    
    p2 <- figure() %>% 
      ly_points(data_store_1$avg_unemployment, data_store_1$avg_sales, data = data_store_1$DATE_OF_WEEK,color = data_store_1$color,hover = list(data_store_1$avg_unemployment, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
      tool_box_select() %>% 
      tool_lasso_select() %>% 
      x_axis(label = "Unemployment") %>% 
      y_axis(label = "Weekly Sales")
    
    p3 <- figure() %>% 
      ly_points(data_store_1$avg_temperature, data_store_1$avg_sales,  data = data_store_1$DATE_OF_WEEK,color = data_store_1$color, hover = list(data_store_1$avg_temperature, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
      tool_box_select() %>% 
      tool_lasso_select() %>% 
      x_axis(label = "Temperature") %>% 
      y_axis(label = "Weekly Sales")
    p4 <- figure() %>% 
      ly_points(data_store_1$avg_fuelprice, data_store_1$avg_sales,  data = data_store_1$DATE_OF_WEEK,color = data_store_1$color, hover = list(data_store_1$sum_markdown, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
      tool_box_select() %>% 
      tool_lasso_select() %>% 
      x_axis(label = "Average Fuel Price") %>% 
      y_axis(label = "Weekly Sales")
    
    p5 <- figure() %>% 
      ly_points(data_store_1$sum_markdown, data_store_1$avg_sales,  data = data_store_1$DATE_OF_WEEK,color = data_store_1$color, hover = list(data_store_1$sum_markdown, data_store_1$avg_sales, data_store_1$DATE_OF_WEEK)) %>% 
      tool_box_select() %>%
      tool_lasso_select() %>% 
      x_axis(label = "Average Promotion") %>% 
      y_axis(label = "Weekly Sales")
    
    p6 <- figure(xaxes = FALSE) %>% 
      ly_crect(x=colors, y=months, color = colors, hover=months) %>% 
      ly_text(x=colors, y=months, text = months, font_style = "bold", font_size = "10pt", align = "center", baseline = "middle")  
    
    grid_plot(list(p1,p2,p3,p4,p5,p6), same_axes = FALSE, link_data = TRUE, nrow=3)
  })
```

ui.R
```
(box(rbokehOutput("scatterPlot", width="100%",height="100%"),width=12))

```

![](avgPlots.png)

Interestingly, we see that the trend of CPI is almost flat, with sudden jump in Weekly sales in both the lower end and the higher end. These sudden jumps all happen in December. Therefore, CPI almost has no effect on weekly sales. Similarly for unemployment, the trend is also flat except for some of the outliers which also happen in December.

![](outliers.png)

The most interest observation is that we initially expected the outliers in December as seen in CPI, Unemployment, temperature, and fuel price plots to have been somehow accounted by an increase in promotions. This, however, is not we saw. While it is true that 3 of the highest weekly sales occur at the higher promotion values (11400), so does a lot of other weeks of the year. Similarly, other 3 of the highest weekly sales also happen when the markdown was twice as smaller in scale. 

Therefore, we conclude that when it is not actually holidays, the month December is actually driving the sales. This could be because more and more people have breaks during those time, or they are shopping to prepare for the new year. 

However, one interesting thing to note that we did not expect is that Black Fridays have less weights then we have thought initially. For example, we would expect Black Fridays to have significant contributions to sales that it would become an outlier. However, we did not see any significant jumps during these Black Friday weeks.

Finally, if we hover over these outliers, we can see the dates of these data points. Again, since these weeks are not holiday weeks, we can see that it's not the holidays that matter, but rather the weeks leading up to the holidays that are driving sales.

![](hoverForMonth.png)

#### Time-Series Analysis of Sales and Related Factors

However, it is also important to analyze which the factors affect sales the most. To do so, we graphed the sales, CPI, unemployment rate, etc. along with time so see how they correlate as a function of time. Note that markdown data is only available after November 2011.

With this plot, it is easier to see that the sales are very cyclical. There are there are 2 peaks toward the end of the year in both 2010 and 2011. The only reasons 2012's peak doesn't show is because the data only goes up to October for 2012. However, from this, you easily gauge the correlation between the different factors and sales.

However, it is obvious now that promotions correlate positively to sales. Other factors, on the other hand, have almost a flat trend and its effect is rather miniscule.

![](dygraph1.png)

Here's another capture of the same graph but with a refined range of date.

![](dygraph2.png)


#### Crosstab Analysis of Store Type and Performance

#### Analysis of Best Performing and Worst Performing Departmental Sales 

Understanding how different stores perform is great. However, we wanted to know why some departments perform better than others and how can we improve them. That's why we specifically pick out the top n stores with the highest average weekly sales and the bottom n stores with the lowest average sales. 

```
    # Find top performing and worst performing departments
    dept_sales_df <- df %>% group_by(DEPT) %>% summarise(AVG_SALES=mean(WEEKLY_SALES)) %>% arrange(desc(AVG_SALES))
    top_n_dept <- as.vector(head(dept_sales_df,n)$DEPT)
    all_dept <- unique(dept_sales_df$DEPT)
    dept_sales_df <- dept_sales_df %>% arrange((AVG_SALES))
    bottom_n_dept <- as.vector(head(dept_sales_df,n)$DEPT)
    
    # Create a new dataframe that only has these top and bottom departments
    newdf2 <- df %>% filter(DEPT %in% c(top_n_dept,bottom_n_dept)) %>% mutate(classification=ifelse(DEPT %in% top_n_dept, "x in top", "x  in bottom"))
    
    # Plot the boxplots with plotly
    plot_ly(y = newdf2$total_markdown, x= newdf2$DEPT, color=newdf2$classification,type = "box")
```

We than created boxplots of average markdown for each department and color the department by whether they are in the top and the bottom. Interesting, we see that the average markdown the top departments are pretty much all equal in the higher end.
![](boxplotAnalysis1.png)
![](boxplotAnalysis2.png)






